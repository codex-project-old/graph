<?php

namespace App\Attributes\Builder;

class FloatNodeDefinition extends \Symfony\Component\Config\Definition\Builder\FloatNodeDefinition
{
    use WithApiTypes;
}
