<?php

namespace App\Attributes\Builder;


class VariableNodeDefinition extends \Symfony\Component\Config\Definition\Builder\VariableNodeDefinition
{
    use WithApiTypes;
}
