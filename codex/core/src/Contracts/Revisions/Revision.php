<?php

namespace Codex\Contracts\Revisions;


/**
 * Interface RevisionInterface
 *
 * @package Codex\Revisions
 * @author  Robin Radic
 * @mixin \Codex\Revisions\Revision
 */
interface Revision
{

}
