<?php


namespace Codex\Contracts\Projects;


/**
 * Interface ProjectInterface
 *
 * @package Codex\Projects
 * @author  Robin Radic
 * @mixin \Codex\Projects\Project
 */
interface Project
{

}
