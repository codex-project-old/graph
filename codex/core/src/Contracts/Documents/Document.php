<?php


namespace Codex\Contracts\Documents;

/**
 * Interface Document
 *
 * @package Codex\Contracts\Documents
 * @author  Robin Radic
 * @mixin \Codex\Documents\Document
 */
interface Document
{

}
