<?php

namespace Codex\Contracts\Mergable;

interface ParentInterface
{
    public function getChildren();

}
