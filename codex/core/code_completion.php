<?php


namespace Illuminate\Support {

    /**
     * This is the class Arr.
     *
     * @package Illuminate\Support
     * @author  Robin Radic
     * @see     \Codex\Support\Arr::explodeToPaths()
     * @method static string[] explodeToPaths($paths, $toArray = false)
     * @method static array merge (array $arr1, array $arr2, $unique = true)
     */
    class Arr
    {

    }
}
